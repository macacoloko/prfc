<?php
require_once 'Conexao.class.php';

	class Pagina {

		public static function buscarTurma(){

			$conexao = Conexao::obterConexao();

			$sql = "SELECT id_turma,desc_turma FROM turma";

			$query = $conexao->query($sql);

			$turmas = $query->fetchAll(PDO::FETCH_ASSOC);

			return $turmas;

		}

		public static function buscarDisciplina($turma){

			$conexao = Conexao::obterConexao();

			$sql = "SELECT possui.id_turma, turma.desc_turma, possui.id_disciplina, disciplina.desc_disciplina FROM possui, turma, disciplina WHERE turma.id_turma=possui.id_turma and disciplina.id_disciplina=possui.id_disciplina and possui.id_turma = $turma ORDER BY desc_disciplina";
			$query = $conexao->query($sql);

			$disciplinas = $query->fetchAll(PDO::FETCH_ASSOC);

			return $disciplinas;
		}

		public static function buscarTodasDisciplinas(){

			$conexao = Conexao::obterConexao();

			$sql = "SELECT id_disciplina, desc_disciplina FROM disciplina ORDER BY desc_disciplina";

			$consulta = $conexao->query($sql);
			
			$disciplinas = $consulta->fetchAll(PDO::FETCH_ASSOC);

			return $disciplinas;

		}

		public static function buscaTurmaUsuario($id_usuario){

			$conexao = Conexao::obterConexao();

			$sql = "SELECT ano.id_turma, desc_turma FROM usuario, ano, turma WHERE ano.id_turma = turma.id_turma AND ano.id_usuario = usuario.id_usuario AND ano.id_usuario = $id_usuario";

			$query = $conexao->query($sql);

			$turmas = $query->fetchAll(PDO::FETCH_ASSOC);

			return $turmas;
		}

		

		public static function buscaDisciplinaUsuario($id_usuario){

			$conexao = Conexao::obterConexao();

			$query = $conexao->query("SELECT id_disciplina FROM prof_disciplina WHERE id_usuario = $id_usuario");

			$disciplinas = $query->fetchAll(PDO::FETCH_ASSOC);

			$result = [];

			foreach ($disciplinas as $id ) {

					$id = $id["id_disciplina"];


					$consulta =  $conexao->query("SELECT id_disciplina, desc_disciplina FROM disciplina WHERE id_disciplina = $id");

					$result[] = $consulta->fetchAll(PDO::FETCH_ASSOC);

			}
			
			return $result;
		}

		public static function buscaPostagemTurma($turma){

			$conexao = Conexao::obterConexao();

				$id_postagem = Pagina::buscaIdPostagem($turma);

				$postagem=[];

				foreach ($id_postagem as $id){

					$id_post=$id['id_postagem'];

					$consulta = $conexao->query("SELECT id_postagem, texto_postagem, data_postagem, hora_postagem, id_usuario FROM postagem WHERE id_postagem = $id_post");

					$postagem[] = $consulta->fetchAll(PDO::FETCH_ASSOC);
					
				}

				return $postagem;

		}

		public static function buscaIdPostagem($turma){

			$conexao = Conexao::obterConexao();

			

			$consulta = $conexao->query("SELECT id_postagem FROM post_turma WHERE id_turma = $turma");

			$post = $consulta->fetchAll(PDO::FETCH_ASSOC);

			return $post;				

		}

		public static function buscaNomePorId($id_usuario){

			$conexao = Conexao::obterConexao();

			$consulta = $conexao->query("SELECT nome FROM usuario WHERE id_usuario = $id_usuario");

			$nome = $consulta->fetch(PDO::FETCH_ASSOC);

			return $nome;

		}

		public static function buscarImagem($id){
        $conexao = Conexao::obterConexao();

        $sql = "SELECT caminho_imagem FROM midia WHERE id_usuario = $id ORDER BY caminho_imagem DESC LIMIT 1";
        
        $consulta = $conexao->query($sql);

        $imagem = $consulta->fetchAll(PDO::FETCH_ASSOC);

        return $imagem;
    }

	}


