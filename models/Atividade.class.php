<?php require_once ('Conexao.class.php');

class Atividade{

	private $texto_atividade;
	private $data_atividade;
	private $tipo_atividade;

	public function cad_atividade($id_usuario, $texto_atividade, $id_turma, $tipo_atividade, $id_disciplina, $data_atividade){

		$conexao = Conexao::obterConexao();

		$this->texto_atividade = $texto_atividade;
		$this->data_atividade  = $data_atividade;
		$this->tipo_atividade  = $tipo_atividade;

		foreach ($id_turma as $turma) {

			foreach ($id_disciplina as $disciplina) {

				$sql = ("INSERT INTO atividade(id_usuario, id_turma, id_disciplina, texto_atividade, data_atividade, tipo_atividade) VALUES($id_usuario, $turma, $disciplina, '$this->texto_atividade', '$this->data_atividade', '$this->tipo_atividade')");

				$conexao->exec($sql);
				
			}

		}

		return true;
	}

	public static function buscaAtividade($id_turma){

		$conexao = Conexao::obterConexao();

			$query = $conexao->query("SELECT id_usuario, id_turma, id_atividade, id_disciplina, tipo_atividade, texto_atividade, data_atividade FROM atividade WHERE id_turma = $id_turma");

			$consulta = $query->fetchAll(PDO::FETCH_ASSOC);

		return $consulta;

	}
	public static function buscaAtividadeProf($id_usuario){

		$conexao = Conexao::obterConexao();

			$query = $conexao->query("SELECT id_usuario, id_turma, id_atividade, id_disciplina, tipo_atividade, texto_atividade, data_atividade FROM atividade WHERE id_usuario = $id_usuario");

			$consulta = $query->fetchAll(PDO::FETCH_ASSOC);

		return $consulta;

	}
	public static function removeAtividade($id_atividade){

		$conexao = Conexao::obterConexao();

		$conexao->exec("DELETE FROM atividade WHERE id_atividade = $id_atividade");

		return true;
	}

}