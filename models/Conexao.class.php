<?php
    class Conexao{

	public static function obterConexao(){
    $usuario   = "root";
    $senha     = "";
    $nome_banco= "virtualclass";

    try  {
        $conexao = new PDO("mysql:host=localhost;dbname=$nome_banco", $usuario, $senha);

        $conexao->query("SET NAMES'utf8'");
        $conexao->query('SET character_set_connection=utf8');
        $conexao->query('SET character_set_client=utf8');
        $conexao->query('SET character_set_results=utf8');

        return $conexao;
    
    } catch(PDOException $erro){
        echo "Conexao falhou: ". $erro->getMessage();
    }
}
}

/*

CREATE TABLE `post_turma` (
    ->   `id_postagem` int(11) DEFAULT NULL,
    ->   `id_turma` int(11) DEFAULT NULL,
    ->   CONSTRAINT `id_postagem` FOREIGN KEY (`id_postagem`) REFERENCES `postagem` (`id_postagem`),
    ->   CONSTRAINT `id_turma` FOREIGN KEY (`id_turma`) REFERENCES `turma` (`id_turma`)
    ->   )  -->

//select possui.id_disciplina, desc_disciplina, possui.id_turma, desc_turma from disciplina, turma, possui where possui.id_disciplina = disciplina.id_disciplina and possui.id_turma = turma.id_turma and possui.id_turma=15;*/