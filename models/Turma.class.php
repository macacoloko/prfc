<?php 
require_once ('Conexao.class.php');
	
class Turma{

	private $desc_turma;

	public function cadastrarTurma($desc_turma, $curso, $disciplinas){

		$this->desc_turma = $desc_turma;
		$this->curso = $curso;
		$this->disciplinas = $disciplinas;

		$conexao = Conexao::obterConexao();

		$query = $conexao->query("SELECT id_turma FROM turma WHERE desc_turma = '$this->desc_turma'");
		$turma = $query->fetchAll(PDO::FETCH_ASSOC);

		if(empty($turma)){

			$sql = "INSERT INTO turma(desc_turma, id_curso) VALUES ('$this->desc_turma', $this->curso)";

			$conexao->exec($sql);

			$query = $conexao->query("SELECT id_turma FROM turma WHERE desc_turma='$this->desc_turma'");

			$id_turma = $query->fetchAll(PDO::FETCH_ASSOC);

			foreach ($this->disciplinas as $disciplina) {

				foreach ($id_turma as $id) {
					
					foreach ($id as $turma) {

						$insere = "INSERT INTO possui(id_turma,id_disciplina) VALUES ($turma,$disciplina)";

						$conexao->exec($insere);
					}
				}
			}

			return true;
		}

	}

	public static function pegarCurso(){

	$conexao = Conexao::obterConexao();

	$sql = $conexao->query("SELECT id_curso, desc_curso FROM curso ORDER BY desc_curso"); 

	$curso = $sql->fetchAll(PDO::FETCH_ASSOC);

	return $curso;

	}

	public static function removerTurma($turma){

		$conexao = Conexao::obterConexao();

		$sql = "DELETE FROM possui WHERE id_turma = $turma";

		$conexao->exec($sql);

		$deletar = "DELETE FROM turma WHERE id_turma = $turma";

		$conexao->exec($deletar);

		return true;
	}

	public static function buscaTurmaPorId($id_turma){

		$conexao = Conexao::obterConexao();

		$query = $conexao->query("SELECT desc_turma FROM turma where id_turma=$id_turma");

		$consulta = $query->fetch(PDO::FETCH_ASSOC);

		return $consulta;

	}
	
}

?>