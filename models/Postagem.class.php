<?php require_once 'Conexao.class.php';
	
	class Postagem{

		public function cadastro($id_usuario, $texto_postagem){

			$conexao = Conexao::obterConexao();

			date_default_timezone_set('America/Sao_Paulo');

			$data = date('Y-m-d');

			$hora = date('H:i');

			$sql = "INSERT INTO postagem(texto_postagem, data_postagem, hora_postagem, id_usuario) VALUES ('$texto_postagem','$data','$hora', $id_usuario)";

			$conexao->exec($sql);

			$query = $conexao->query("SELECT id_postagem FROM postagem WHERE id_usuario ORDER BY data_postagem DESC, hora_postagem DESC LIMIT 1");

			$id_postagem = $query->fetch(PDO::FETCH_ASSOC);
			
			return $id_postagem;
		}

		public static function adicionar_conteudo($caminho, $id_postagem, $name){

			$conexao = Conexao::obterConexao();

			foreach ($id_postagem as $id) {

				$conexao->exec("INSERT INTO midia(caminho_imagem, id_postagem, nome_imagem) VALUES ('$caminho', $id, '$name')");
			}	

			return true;

		}

		public function relacionaPostagem($turmas, $id_postagem, $disciplinas){

			$conexao = Conexao::obterConexao();

			foreach ($id_postagem as $postagem) {

				foreach ($disciplinas as $disciplina) {

					foreach ($turmas as $turma) {

						$sql = "INSERT INTO post_turma(id_turma, id_postagem, id_disciplina) VALUES ($turma, $postagem, $disciplina)";

						

						$conexao->exec($sql);
					}
				}		
			}

			return true;
		}

		public static function postagemProf($id_usuario){

			$conexao = Conexao::obterConexao();

			$query = $conexao->query("SELECT id_postagem, texto_postagem, data_postagem, hora_postagem  FROM postagem WHERE id_usuario = $id_usuario ORDER BY data_postagem DESC, hora_postagem DESC");

			$dados_postagem = $query->fetchAll(PDO::FETCH_ASSOC);

			return $dados_postagem;

		}

		public function cad_comentario($id_usuario, $texto_comentario, $id_postagem){

			$conexao = Conexao::obterConexao();

			$this->id_usuario =       $id_usuario;
			$this->texto_comentario = $texto_comentario;
			$this->id_postagem =      $id_postagem;

			date_default_timezone_set('America/Sao_Paulo');

			$data = date('Y-m-d');

			$hora = date('H:i');

			$sql = ("INSERT INTO comentario(texto_comentario, data_comentario, hora_comentario, id_postagem, id_usuario) VALUES ('$this->texto_comentario', '$data', '$hora', '$this->id_postagem', '$this->id_usuario')");

			$conexao->exec($sql);

			return true;

		}

		public static function buscaComentario($id_postagem){

			$conexao = Conexao::obterConexao();

			$query = $conexao->query("SELECT id_comentario, texto_comentario, data_comentario, hora_comentario, id_usuario FROM comentario WHERE id_postagem = $id_postagem");

			$consulta = $query->fetchAll(PDO::FETCH_ASSOC);

			return $consulta;

		}

		public static function deletar_postagem($id_postagem){

			$conexao = Conexao::obterConexao();

			$conexao->exec("DELETE FROM post_turma WHERE id_postagem = $id_postagem");
			$conexao->exec("DELETE FROM midia WHERE id_postagem = $id_postagem");
			$conexao->exec("DELETE FROM postagem WHERE id_postagem = $id_postagem");
			$conexao->exec("DELETE FROM comentario WHERE id_postagem = $id_postagem");
			
			return true;
		}
		public static function deletar_comentario($id_comentario){

			$conexao = Conexao::obterConexao();

			$conexao->exec("DELETE FROM comentario WHERE id_comentario = $id_comentario");

			return true;
		}

		public static function alterar_post($id_postagem, $texto_postagem){

			$conexao = Conexao::obterConexao();

			date_default_timezone_set('America/Sao_Paulo');

			$data = date('Y-m-d');

			$hora = date('H:i');

			$conexao->exec("UPDATE postagem SET texto_postagem = '$texto_postagem', data_postagem = '$data', hora_postagem = '$hora' WHERE id_postagem = $id_postagem");

			return true;

		}
		public static function alterar_coment($id_comentario, $texto_comentario){

			$conexao = Conexao::obterConexao();

			date_default_timezone_set('America/Sao_Paulo');

			$data = date('Y-m-d');

			$hora = date('H:i');

			$conexao->exec("UPDATE comentario SET texto_comentario = '$texto_comentario', data_postagem = '$data', hora_postagem = '$hora' WHERE id_comentario = $id_comentario");

			return true;

		}
		public static function buscaMidiaPost($id_postagem){

	        $conexao = Conexao::obterConexao();

	        	$sql = "SELECT caminho_imagem FROM midia WHERE id_postagem = $id_postagem";
	        
	        	$consulta = $conexao->query($sql);

	        	$midia = $consulta->fetchAll(PDO::FETCH_ASSOC);

	        return $midia;
    	}
	}

	