-- MySQL dump 10.16  Distrib 10.1.22-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: virtualclass
-- ------------------------------------------------------
-- Server version	10.1.22-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ano`
--

DROP TABLE IF EXISTS `ano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ano` (
  `id_turma` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `ano` int(4) DEFAULT NULL,
  KEY `id_turma` (`id_turma`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `ano_ibfk_1` FOREIGN KEY (`id_turma`) REFERENCES `turma` (`id_turma`),
  CONSTRAINT `ano_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ano`
--

LOCK TABLES `ano` WRITE;
/*!40000 ALTER TABLE `ano` DISABLE KEYS */;
INSERT INTO `ano` VALUES (15,1,2017),(12,2,2017),(13,2,2017),(14,2,2017),(15,2,2017),(16,2,2017),(1,2,2017),(9,3,2017),(10,3,2017),(11,3,2017),(1,3,2017),(1,7,2017),(1,7,2017),(10,9,2017),(6,9,2017),(2,10,2017),(1,10,2017),(16,11,2017),(1,13,2017);
/*!40000 ALTER TABLE `ano` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atividade`
--

DROP TABLE IF EXISTS `atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atividade` (
  `id_usuario` int(11) DEFAULT NULL,
  `id_turma` int(11) DEFAULT NULL,
  `id_disciplina` int(11) DEFAULT NULL,
  `id_atividade` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_atividade` varchar(20) DEFAULT NULL,
  `texto_atividade` varchar(40) DEFAULT NULL,
  `data_atividade` date DEFAULT NULL,
  PRIMARY KEY (`id_atividade`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_turma` (`id_turma`),
  KEY `id_disciplina` (`id_disciplina`),
  CONSTRAINT `atividade_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `atividade_ibfk_2` FOREIGN KEY (`id_turma`) REFERENCES `turma` (`id_turma`),
  CONSTRAINT `atividade_ibfk_3` FOREIGN KEY (`id_disciplina`) REFERENCES `disciplina` (`id_disciplina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividade`
--

LOCK TABLES `atividade` WRITE;
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentario`
--

DROP TABLE IF EXISTS `comentario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentario` (
  `id_comentario` int(11) NOT NULL AUTO_INCREMENT,
  `texto_comentario` varchar(40) NOT NULL,
  `data_comentario` date DEFAULT NULL,
  `hora_comentario` time DEFAULT NULL,
  `id_postagem` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_comentario`),
  KEY `id_postagem` (`id_postagem`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `comentario_ibfk_1` FOREIGN KEY (`id_postagem`) REFERENCES `postagem` (`id_postagem`),
  CONSTRAINT `comentario_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentario`
--

LOCK TABLES `comentario` WRITE;
/*!40000 ALTER TABLE `comentario` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `id_curso` int(11) NOT NULL AUTO_INCREMENT,
  `desc_curso` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (1,'Agropecuaria'),(2,'Informatica'),(3,'Quimica');
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplina`
--

DROP TABLE IF EXISTS `disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplina` (
  `id_disciplina` int(11) NOT NULL AUTO_INCREMENT,
  `desc_disciplina` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_disciplina`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplina`
--

LOCK TABLES `disciplina` WRITE;
/*!40000 ALTER TABLE `disciplina` DISABLE KEYS */;
INSERT INTO `disciplina` VALUES (1,'LÍNGUA PORTUGUESA'),(2,'ARTES'),(3,'LÍNGUA ESTRANGEIRA M'),(4,'LÍNGUA ESTRANGEIRA M'),(5,'FÍSICA'),(6,'GEOGRAFIA'),(7,'BIOLOGIA'),(8,'FILOSOFIA'),(9,'EDUCAÇÃO FÍSICA'),(10,'SOCIOLOGIA'),(11,'ÉTICA E MEIO AMBIENT'),(12,'MATEMÁTICA'),(13,'HISTÓRIA'),(14,'QUÍMICA'),(15,'PRODUÇÃO TEXTUAL'),(16,'PRÁTICA PROFISSIONAL'),(17,'ZOOTECNIA I'),(18,'MECANIZAÇÃO AGRÍCOLA'),(19,'AGRICULTURA I'),(20,'AGROECOLOGIA'),(21,'DESENHOS TEC. CONSTR'),(22,'IRRIGAÇÃO E DRENAGEM'),(23,'AGRICULTURA II'),(24,'TOPOGRAFIA'),(25,'DEFESA FITOSSANITÁRI'),(26,'ZOOTECNIA II'),(27,'PRODUTOS AGROINDUSTR'),(28,'ZOOTECNIA III'),(29,'AGRICULTURA III'),(30,'ADMINISTRAÇÃO RURAL'),(31,'QUÍMICA GERAL'),(32,'QUÍMICA INORGÂNICA I'),(33,'METODOLOGIA DA PESQU'),(34,'FÍSICO-QUÍMICA I'),(35,'QUÍMICA ORGÂNICA I'),(36,'QUÍMICA INORGÂNICA I'),(37,'OPERAÇÕES UNITÁRIAS'),(38,'BIOQUÍMICA'),(39,'QUÍMICA ORGÂNICA II'),(40,'FÍSICO-QUÍMICA II'),(41,'QUÍMICA ANALÍTICA QU'),(42,'QUÍMICA AMBIENTAL'),(43,'QUÍMICA ANALÍTICA QU'),(44,'GESTÃO E EMPREENDEDO'),(45,'QUÍMICA ANALÍTICA IN'),(46,'MICROBIOLOGIA'),(47,'QUÍMICA TECNOLÓGICA'),(48,'TRATAMENTO DE ÁGUAS '),(49,'PROJETO INTEGRADOR'),(50,'CORROSÃO E TRATAMENT'),(51,'PROJ. INTEGRADOR'),(52,'MULTIMÍDIA'),(53,'DESENVOLVIMENTO WEB'),(54,'FUND. INFORMÁTICA'),(55,'LÓGICA DE PROGRAMAÇÃ'),(56,'ENG. SOFTWARE'),(57,'HARDWARE E SISTEMAS '),(58,'BANCO DE DADOS'),(59,'PROGRAMAÇÃO'),(60,'GESTÃO DE NEGÓCIOS'),(61,'SEGURANÇA DE SISTEMA'),(62,'QUALIDADE DE SOFTWAR'),(63,'PRÁTICAS PROFISSIONA'),(64,'ÉTICA DE MEIO AMBIEN'),(65,'PROJETO FINAL DE CUR');
/*!40000 ALTER TABLE `disciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `midia`
--

DROP TABLE IF EXISTS `midia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `midia` (
  `id_midia` int(11) NOT NULL AUTO_INCREMENT,
  `id_postagem` int(11) DEFAULT NULL,
  `nome_imagem` varchar(80) NOT NULL,
  `caminho_imagem` varchar(80) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_midia`),
  KEY `id_postagem` (`id_postagem`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `imagens_ibfk_1` FOREIGN KEY (`id_postagem`) REFERENCES `postagem` (`id_postagem`),
  CONSTRAINT `imagens_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `midia`
--

LOCK TABLES `midia` WRITE;
/*!40000 ALTER TABLE `midia` DISABLE KEYS */;
INSERT INTO `midia` VALUES (7,NULL,'2017-10-06-02-19-30.jpeg','views/assets/images/2017-10-06-02-19-30.jpeg',1),(8,NULL,'2017-10-06-02-19-47.jpeg','views/assets/images/2017-10-06-02-19-47.jpeg',1),(9,NULL,'2017-10-06-02-20-22.jpeg','views/assets/images/2017-10-06-02-20-22.jpeg',1),(10,NULL,'2017-10-06-02-24-51.jpeg','views/assets/images/2017-10-06-02-24-51.jpeg',2);
/*!40000 ALTER TABLE `midia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `possui`
--

DROP TABLE IF EXISTS `possui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `possui` (
  `id_turma` int(11) DEFAULT NULL,
  `id_disciplina` int(11) DEFAULT NULL,
  KEY `id_turma` (`id_turma`),
  KEY `id_disciplina` (`id_disciplina`),
  CONSTRAINT `possui_ibfk_1` FOREIGN KEY (`id_turma`) REFERENCES `turma` (`id_turma`),
  CONSTRAINT `possui_ibfk_2` FOREIGN KEY (`id_disciplina`) REFERENCES `disciplina` (`id_disciplina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `possui`
--

LOCK TABLES `possui` WRITE;
/*!40000 ALTER TABLE `possui` DISABLE KEYS */;
INSERT INTO `possui` VALUES (1,16),(1,15),(1,13),(1,1),(1,14),(1,12),(1,8),(1,7),(1,4),(1,10),(1,17),(1,6),(1,19),(1,18),(1,3),(1,5),(1,9),(1,20),(1,21),(2,16),(2,15),(2,13),(2,1),(2,14),(2,12),(2,8),(2,7),(2,4),(2,10),(2,17),(2,6),(2,19),(2,18),(2,3),(2,5),(2,9),(2,20),(2,21),(3,16),(3,15),(3,13),(3,1),(3,14),(3,12),(3,8),(3,7),(3,4),(3,10),(3,17),(3,6),(3,19),(3,18),(3,3),(3,5),(3,9),(3,20),(3,21),(4,8),(4,22),(4,9),(4,23),(4,10),(4,24),(4,2),(4,7),(4,1),(4,25),(4,5),(4,14),(4,26),(4,12),(4,4),(4,3),(4,16),(4,6),(4,13),(5,8),(5,22),(5,9),(5,23),(5,10),(5,24),(5,2),(5,7),(5,1),(5,25),(5,5),(5,14),(5,26),(5,12),(5,4),(5,3),(5,16),(5,6),(5,13),(6,8),(6,22),(6,9),(6,23),(6,10),(6,24),(6,2),(6,7),(6,1),(6,25),(6,5),(6,14),(6,26),(6,12),(6,4),(6,3),(6,16),(6,6),(6,13),(7,16),(7,27),(7,7),(7,5),(7,6),(7,28),(7,4),(7,3),(7,9),(7,29),(7,12),(7,8),(7,2),(7,14),(7,10),(7,15),(7,1),(7,13),(7,63),(8,16),(8,27),(8,7),(8,5),(8,6),(8,28),(8,4),(8,3),(8,9),(8,29),(8,12),(8,8),(8,2),(8,14),(8,10),(8,15),(8,1),(8,13),(8,63),(9,49),(9,7),(9,12),(9,50),(9,3),(9,14),(9,8),(9,2),(9,7),(9,51),(9,9),(9,13),(9,6),(9,1),(9,12),(9,4),(9,52),(9,15),(9,14),(9,10),(9,5),(10,49),(10,7),(10,12),(10,50),(10,3),(10,14),(10,8),(10,2),(10,7),(10,51),(10,9),(10,13),(10,6),(10,1),(10,12),(10,4),(10,52),(10,15),(10,14),(10,10),(10,5),(11,49),(11,7),(11,12),(11,50),(11,3),(11,14),(11,8),(11,2),(11,7),(11,51),(11,9),(11,13),(11,6),(11,1),(11,12),(11,4),(11,52),(11,15),(11,14),(11,10),(11,5),(12,1),(12,5),(12,7),(12,3),(12,4),(12,8),(12,9),(12,6),(12,12),(12,64),(12,53),(12,54),(12,55),(12,10),(12,56),(12,14),(12,13),(13,1),(13,5),(13,7),(13,3),(13,4),(13,8),(13,9),(13,6),(13,12),(13,64),(13,53),(13,54),(13,55),(13,10),(13,56),(13,14),(13,13),(14,1),(14,5),(14,7),(14,3),(14,4),(14,8),(14,9),(14,6),(14,12),(14,64),(14,53),(14,54),(14,55),(14,10),(14,56),(14,14),(14,13),(15,1),(15,3),(15,4),(15,5),(15,57),(15,6),(15,58),(15,7),(15,8),(15,9),(15,55),(15,10),(15,59),(15,60),(15,61),(15,12),(15,62),(15,13),(15,14),(16,1),(16,3),(16,4),(16,5),(16,57),(16,6),(16,58),(16,7),(16,8),(16,9),(16,55),(16,10),(16,59),(16,60),(16,61),(16,12),(16,62),(16,13),(16,14),(17,4),(17,9),(17,6),(17,3),(17,5),(17,13),(17,12),(17,15),(17,30),(17,8),(17,2),(17,7),(17,1),(17,31),(17,10),(18,1),(18,8),(18,32),(18,12),(18,33),(18,34),(18,3),(18,4),(18,7),(18,5),(18,9),(18,13),(18,10),(18,35),(18,6),(19,36),(19,3),(19,4),(19,12),(19,9),(19,37),(19,2),(19,38),(19,7),(19,10),(19,39),(19,6),(19,5),(19,1),(19,13),(19,40),(19,41),(19,8),(20,6),(20,15),(20,42),(20,43),(20,44),(20,10),(20,45),(20,12),(20,65),(20,46),(20,47),(20,8),(20,48);
/*!40000 ALTER TABLE `possui` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_turma`
--

DROP TABLE IF EXISTS `post_turma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_turma` (
  `id_postagem` int(11) DEFAULT NULL,
  `id_turma` int(11) DEFAULT NULL,
  `id_disciplina` int(11) DEFAULT NULL,
  KEY `id_postagem` (`id_postagem`),
  KEY `id_turma` (`id_turma`),
  KEY `id_disciplina` (`id_disciplina`),
  CONSTRAINT `id_postagem` FOREIGN KEY (`id_postagem`) REFERENCES `postagem` (`id_postagem`),
  CONSTRAINT `id_turma` FOREIGN KEY (`id_turma`) REFERENCES `turma` (`id_turma`),
  CONSTRAINT `post_turma_ibfk_1` FOREIGN KEY (`id_disciplina`) REFERENCES `disciplina` (`id_disciplina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_turma`
--

LOCK TABLES `post_turma` WRITE;
/*!40000 ALTER TABLE `post_turma` DISABLE KEYS */;
INSERT INTO `post_turma` VALUES (1,15,16),(1,16,16),(2,15,16);
/*!40000 ALTER TABLE `post_turma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postagem`
--

DROP TABLE IF EXISTS `postagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postagem` (
  `id_postagem` int(11) NOT NULL AUTO_INCREMENT,
  `texto_postagem` varchar(100) DEFAULT NULL,
  `data_postagem` date DEFAULT NULL,
  `hora_postagem` time DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_postagem`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `postagem_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postagem`
--

LOCK TABLES `postagem` WRITE;
/*!40000 ALTER TABLE `postagem` DISABLE KEYS */;
INSERT INTO `postagem` VALUES (1,'postagem','2017-10-01','16:13:00',2),(2,'só p/ 3info1','2017-10-03','21:29:00',2);
/*!40000 ALTER TABLE `postagem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prof_disciplina`
--

DROP TABLE IF EXISTS `prof_disciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prof_disciplina` (
  `id_disciplina` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `ano` int(5) NOT NULL,
  KEY `id_disciplina` (`id_disciplina`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `prof_disciplina_ibfk_1` FOREIGN KEY (`id_disciplina`) REFERENCES `disciplina` (`id_disciplina`),
  CONSTRAINT `prof_disciplina_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prof_disciplina`
--

LOCK TABLES `prof_disciplina` WRITE;
/*!40000 ALTER TABLE `prof_disciplina` DISABLE KEYS */;
INSERT INTO `prof_disciplina` VALUES (16,2,2017),(49,2,2017),(55,3,2017),(2,7,2017),(2,10,2017);
/*!40000 ALTER TABLE `prof_disciplina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resposta`
--

DROP TABLE IF EXISTS `resposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resposta` (
  `id_resposta` int(11) NOT NULL AUTO_INCREMENT,
  `texto_resposta` varchar(40) NOT NULL,
  `data_resposta` date DEFAULT NULL,
  `hora_resposta` time DEFAULT NULL,
  `id_comentario` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_resposta`),
  KEY `id_comentario` (`id_comentario`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `resposta_ibfk_1` FOREIGN KEY (`id_comentario`) REFERENCES `comentario` (`id_comentario`),
  CONSTRAINT `resposta_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resposta`
--

LOCK TABLES `resposta` WRITE;
/*!40000 ALTER TABLE `resposta` DISABLE KEYS */;
/*!40000 ALTER TABLE `resposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tip_usuario`
--

DROP TABLE IF EXISTS `tip_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tip_usuario` (
  `id_tip_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `desc_tip_usuario` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_tip_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tip_usuario`
--

LOCK TABLES `tip_usuario` WRITE;
/*!40000 ALTER TABLE `tip_usuario` DISABLE KEYS */;
INSERT INTO `tip_usuario` VALUES (1,'aluno'),(2,'professor'),(3,'administrador');
/*!40000 ALTER TABLE `tip_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turma`
--

DROP TABLE IF EXISTS `turma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turma` (
  `id_turma` int(11) NOT NULL AUTO_INCREMENT,
  `desc_turma` varchar(20) DEFAULT NULL,
  `id_curso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_turma`),
  KEY `id_curso` (`id_curso`),
  CONSTRAINT `turma_ibfk_1` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turma`
--

LOCK TABLES `turma` WRITE;
/*!40000 ALTER TABLE `turma` DISABLE KEYS */;
INSERT INTO `turma` VALUES (1,'1AGROI1',1),(2,'1AGROI2',1),(3,'1AGROI3',1),(4,'2AGROI1',1),(5,'2AGROI2',1),(6,'2AGROI3',1),(7,'3AGROI1',1),(8,'3AGROI2',1),(9,'1INFOI1',2),(10,'1INFOI2',2),(11,'1INFOI3',2),(12,'2INFOI1',2),(13,'2INFOI2',2),(14,'2INFOI3',2),(15,'3INFOI1',2),(16,'3INFOI2',2),(17,'1QUIMI',3),(18,'2QUIMI',3),(19,'3QUIMI',3),(20,'4QUIMI',3);
/*!40000 ALTER TABLE `turma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `nome` varchar(20) NOT NULL,
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `senha` varchar(15) NOT NULL,
  `email` varchar(20) NOT NULL,
  `login` varchar(20) NOT NULL,
  `data_nasc` date DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `id_tip_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `id_tip_usuario` (`id_tip_usuario`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_tip_usuario`) REFERENCES `tip_usuario` (`id_tip_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('Gabriela Andrade',1,'123','gabe@gabe.com','gabe123','2000-01-05','representa',1),('Fabio',2,'123','fabio@fabio.com','fabio123','1980-09-08','professor',2),('Jefferson',3,'123','jefferson.chaves@ifc','jefferson','1989-04-26','regente',2),('Ivo',7,'123','ivo.riegel@ifc.edu.b','ivo','1985-12-17','regente',2),('Moissa',8,'123','moissa@ifc.edu.br','moissa','1998-10-23','regente',2),('Gabi',9,'123','gani@ifc.edu.br','gabi','1998-12-12','aluno',1),('Grasi ',10,'123','grasi@grasi.com','grasi','1989-02-21','regente',2),('Edi Ilson',11,'123','edi@edi.com','edijr','1998-12-13','aluno',1),('Moderador',12,'admin123','admin@admin.com','admin','2000-01-05','administra',3),('alunoagro',13,'123','alunoagro@gmail.com','alunoagro','1998-08-05','aluno',1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-05 21:47:05
