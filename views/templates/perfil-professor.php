<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title>Virtual Class</title>

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/estilo.css" rel="stylesheet" type="text/css"/>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>


    </head>


    <body>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.html" class="logo"><span>Virtual<span>Class</span></span></a>
                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li>
                                <form role="search" class="navbar-left app-search pull-left hidden-xs">
                                     <input type="text" placeholder="Search..." class="form-control">
                                     <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                            <li>
                                <!-- Notification -->
                                
                                <!-- End Notification bar -->
                            </li>

                            <li class="dropdown user-box">
                                <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown" aria-expanded="true">
                                    <img src="assets/images/profile.jpg" alt="user-img" class="img-circle user-img">
                                    <div class="user-status away"><i class="zmdi zmdi-dot-circle"></i></div>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Perfil </a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Configurações </a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Bloquear tela</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Sair </a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>

                </div>
            </div>

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <li>
                                <a href="index.html"><i class="zmdi zmdi-home"></i> <span> Início </span> </a>
                            </li>
                            <li class="has-submenu">
                                <a href="#"><i class="zmdi zmdi-accounts-list"></i> <span> Turmas </span> </a>
                                <ul class="submenu megamenu">
                                    <li>
                                        <ul>
                                            <li><a href="ui-buttons.html">1Info1</a></li>
                                            <li><a href="ui-cards.html">1Info2</a></li>
                                            <li><a href="ui-draggable-cards.html">1Info3</a></li>
                                            <li><a href="ui-typography.html">2Info1</a></li>
                                            <li><a href="ui-checkbox-radio.html">2Info2</a></li>
                                            <li><a href="ui-material-icons.html">2Info3</a></li>
                                            <li><a href="ui-font-awesome-icons.html">3Info1</a></li>
                                            <li><a href="ui-themify-icons.html">3Info2</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- End navigation menu  -->
                    </div>
                </div>
            </div>
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper topo">
            <div class="container">

                <!-- Page-Title -->
                

                <div class="row">
                    <div class="col-sm-8">
                        <div class="bg-picture card-box">
                            <div class="profile-info-name">
                                <img src="assets/images/profile.jpg"
                                     class="img-thumbnail" alt="profile-image">

                                <div class="profile-info-detail">
                                    <h3 class="m-t-0 m-b-0"> Usuário </h3>
                                    <p class="text-muted m-b-20"><i>Professor de Programação</i></p>
                                    <p>Hi I'm Alexandra Clarkson,has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC,making it over 2000 years old.Contrary to popular belief, Lorem Ipsum is not simplyrandom text. It has roots in a piece of classical Latin literature from 45 BC.</p>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!--/ meta -->



                        <form method="post" class="card-box">
                            <span class="input-icon icon-right">
                                <textarea rows="2" class="form-control" placeholder="Post a new message"></textarea>
                            </span>
                            <div class="p-t-10 pull-right">
                                <a class="btn btn-sm btn-primary waves-effect waves-light">Send</a>
                            </div>
                            <ul class="nav nav-pills profile-pills m-t-10">
                                <li>
                                    <a href="#"><i class="fa fa-user"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-location-arrow"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class=" fa fa-camera"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-smile-o"></i></a>
                                </li>
                            </ul>

                        </form>
                        <div class="card-box">
                            <div class="comment">
                                <img src="assets/images/profile.jpg" alt="" class="comment-avatar">
                                <div class="comment-body">
                                    <div class="comment-text">
                                        <div class="comment-header">
                                            <a href="#" title="">Usuário</a><span>about 2 minuts ago</span>
                                        </div>
                                        Slides sobre frameworks

                                        <div class="m-t-15">
                                            <a href="">
                                                <img src="assets/images/slides-icon.png" class="thumb-md">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="comment-footer">
                                        <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                        <a href="#"><i class="fa fa-thumbs-o-down"></i></a>
                                        <a href="#">Reply</a>
                                    </div>
                                </div>

                                <div class="comment">
                                    <img src="assets/images/users/avatar-11.jpg" alt="" class="comment-avatar">
                                    <div class="comment-body">
                                        <div class="comment-text">
                                            <div class="comment-header">
                                                <a href="#" title="">Aluna</a><span>about 1 hour ago</span>
                                            </div>
                                            Bla bla bla
                                        </div>
                                        <div class="comment-footer">
                                            <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                            <a href="#"><i class="fa fa-thumbs-o-down"></i></a>
                                            <a href="#">Reply</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="comment">
                                    <img src="assets/images/users/avatar-12.jpg" alt="" class="comment-avatar">
                                    <div class="comment-body">
                                        <div class="comment-text">
                                            <div class="comment-header">
                                                <a href="#" title="">Aluno</a><span>about 2 hours ago</span>
                                            </div>
                                            Bla bla bla bla bla
                                        </div>
                                        <div class="comment-footer">
                                            <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                            <a href="#"><i class="fa fa-thumbs-o-down"></i></a>
                                            <a href="#">Reply</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment">
                                <img src="assets/images/profile.jpg" alt="" class="comment-avatar">
                                <div class="comment-body">
                                    <div class="comment-text">
                                        <div class="comment-header">
                                            <a href="#" title="">Usuário</a><span>about 4 hours ago</span>
                                        </div>
                                        Documento com o trabalho para dia 04/04
                                        <div class="m-t-15">
                                            <a href="">
                                                <img src="assets/images/pdf-icon.png" class="thumb-md">
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div class="comment-footer">
                                        <a href="#"><i class="fa fa-thumbs-o-up"></i></a>
                                        <a href="#"><i class="fa fa-thumbs-o-down"></i></a>
                                        <a href="#">Reply</a>
                                    </div>
                                </div>
                            </div>

                            <div class="m-t-30 text-center">
                                <a href="" class="btn btn-default waves-effect waves-light btn-sm">Load More...</a>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-4">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Professores</h4>

                            <ul class="list-group m-b-0 user-list">
                                <li class="list-group-item">
                                    <a href="#" class="user-list-item">
                                        <div class="avatar">
                                            <img src="assets/images/users/avatar-2.jpg" alt="">
                                        </div>
                                        <div class="user-desc">
                                            <span class="name">Michael Zenaty</span>
                                            <span class="desc">CEO</span>
                                        </div>
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <a href="#" class="user-list-item">
                                        <div class="avatar">
                                            <img src="assets/images/users/avatar-3.jpg" alt="">
                                        </div>
                                        <div class="user-desc">
                                            <span class="name">James Neon</span>
                                            <span class="desc">Web Designer</span>
                                        </div>
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <a href="#" class="user-list-item">
                                        <div class="avatar">
                                            <img src="assets/images/users/avatar-5.jpg" alt="">
                                        </div>
                                        <div class="user-desc">
                                            <span class="name">John Smith</span>
                                            <span class="desc m-b-0">Web Developer</span>
                                        </div>
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <a href="#" class="user-list-item">
                                        <div class="avatar">
                                            <img src="assets/images/users/avatar-6.jpg" alt="">
                                        </div>
                                        <div class="user-desc">
                                            <span class="name">Michael Zenaty</span>
                                            <span class="desc">Programmer</span>
                                        </div>
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <a href="#" class="user-list-item">
                                        <div class="avatar">
                                            <img src="assets/images/users/avatar-1.jpg" alt="">
                                        </div>
                                        <div class="user-desc">
                                            <span class="name">Mat Helme</span>
                                            <span class="desc">Manager</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                </div>
                <!-- end row -->


                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->



            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="zmdi zmdi-close-circle-o"></i>
                </a>
                <h4 class="">Notifications</h4>
                <div class="notification-list nicescroll">
                    <ul class="list-group list-no-border user-list">
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">Michael Zenaty</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-info">
                                    <i class="zmdi zmdi-account"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Signup</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">5 hours ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-pink">
                                    <i class="zmdi zmdi-comment"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">New Message received</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <div class="user-desc">
                                    <span class="name">James Anderson</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">2 days ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a href="#" class="user-list-item">
                                <div class="icon bg-warning">
                                    <i class="zmdi zmdi-settings"></i>
                                </div>
                                <div class="user-desc">
                                    <span class="name">Settings</span>
                                    <span class="desc">There are new settings available</span>
                                    <span class="time">1 day ago</span>
                                </div>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>


        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>