<h4 class="header-title m-t-0 m-b-30">Cadastrar atividade</h4>
<form action="../controller/atividade_controller.php" method="POST" class="card-box">
	<ul class="nav nav-pills profile-pills m-t-10">
		<div class="form-group clearfix">
            <label class="col-lg-2 control-label " for="surname"> Descrição </label>
            <div class="col-lg-10 post">
                <input id="descricao" name="descricao" type="text" class="required form-control" placeholder="Descrição opcional">
            </div>
        </div>
        <div class="form-group clearfix">
            <label class="col-lg-2 control-label " for="surname"> Turmas </label>
	        <div class="col-sm-4 post">
		        <select class="select2 select2-multiple" multiple="multiple" multiple data-placeholder="Turmas" id="select_turma" name='turma[]'>

		            <?php

		            foreach (buscaTurmaUsuario($_SESSION['id_usuario']) as $turmas) { ?>

		            <option value= '<?= $turmas['id_turma'];?>'> <?=$turmas['desc_turma']; ?></option>

		            <?php } ?>

		        </select>
	    	</div>
	    	<label class="col-lg-2 control-label " for="surname"> Tipo ativ.</label>
	    	<div class="col-sm-4 post">
		        <select class="form-control select2" placeholder="disciplina"  id="select_disciplina" name='tipo_atividade'>
		            <option value="Prova"> Prova </option>
		            <option value="Trabalho"> Trabalho </option>
		        </select>
	    	</div>
        </div>
		<div class="form-group clearfix">
            <label class="col-lg-2 control-label " for="surname"> Dsiciplina </label>
            <div class="col-sm-4 post">
		        <select class="form-control select2" placeholder="disciplina"  id="select_disciplina" name='disciplina[]'>

		        <?php

		            foreach (buscaDisciplinaUsuario($_SESSION['id_usuario']) as $disciplina): ?>

		                <option value= '<?= $disciplina[0]['id_disciplina'];?>'> <?=$disciplina[0]['desc_disciplina']; ?></option>

		            <?php endforeach ?>       

		        </select>
	    	</div>
	    	<label class="col-lg-2 control-label " for="Date"> Data ativ. </label>
            <div class="col-sm-4 post">
                <input name="data_atividade" type="date" placeholder="" class="form-control">
            </div>
        </div>
        <input type="hidden" name="rota" value="cad_atividade">
        <ul class="pager wizard m-b-0">
            <button class="btn btn-inverse btn-bordred waves-effect w-md waves-light m-b-5 cad_atividade" type="Submit"> Cadastrar </button>
        </ul>

	</ul>
</form>