<section class="col-sm-8 form">
    <h4 class="header-title m-t-0 m-b-30">Turmas</h4>
    <form class="form-horizontal" role="form" action="../controller/turma_controller.php" method="POST" data-parsley-validate novalidate>
        <div class="form-group clearfix">
            <label class="col-sm-4 control-label"> Turma(s) </label>
            <div class="col-sm-7">
                <select class="form-control select2" data-placeholder="Turmas" id="select_turma" name='remove_turma'>
                    
                        <?php                                                                  
                        foreach (pegarTurma() as $turma) {?>
                           
                        <option value= '<?= $turma['id_turma'];?>'> <?= utf8_encode($turma['desc_turma']); ?></option>

                        <?php } ?>
                        
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden" name="rota" value="removerTurma"> 
                <button type="submit" class="btn btn-primary waves-effect waves-light"> Remover </button>
                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5"> Cancelar </button>
            </div>
        </div>
    </form>
</section>