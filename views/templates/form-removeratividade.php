<h4 class="header-title m-t-0 m-b-30">Remover atividade</h4>

<?php  foreach (buscaAtividadeProf($_SESSION['id_usuario']) as $atividades) { ?>
<ul class="list-group list-no-border user-list">
    <li class="list-group-item">
        <a href="#" class="user-list-item">
            <div class="user-desc">
                <span class="name"><?php 
                $desc = pegarDisciplinaPorId($atividades['id_disciplina']);

                foreach ($desc as $disciplina) {
                   
                echo "Atividade: ".$atividades['tipo_atividade']." de ".strtolower($disciplina['desc_disciplina']);   

     
                } ?>
                    
                </span>

                <span class="desc">
                <?php 
                    $desc_turma = buscaTurmaPorId($atividades['id_turma']);
                    echo "Turma: ".$desc_turma['desc_turma'];
                ?>
                </span>
                <span class="time notificacao"><?="Data: ".date('d/m/Y', strtotime($atividades['data_atividade'])); ?></span>

            </div>
            <form action="../controller/atividade_controller.php" method="post">

            <input type="hidden" name="id_atividade" value=<?=$atividades['id_atividade']?>>

            <input type="hidden" name="rota" value="remove_atividade">

            <button type="submit" class="btn btn-danger waves-effect  btn-xs w-xs waves-light m-b-5 remove_atividade"> <a class="admin" href="inicial.php?pos=1&pgs=templates/form-removerdisciplina.php&id=formremoverdisciplina"> Remover </button>
            </form>
        </a>

    </li>
</ul>
<?php } ?>
