<section class="col-sm-8 form">
    <h4 class="header-title m-t-0 m-b-30">Curso</h4>
    <form class="form-horizontal" role="form" action="../controller/curso_controller.php" method="POST" data-parsley-validate novalidate>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Curso</label>
            <div class="col-sm-7">
                <input type="text" name="curso" required parsley-type="text" class="form-control" id="inputEmail3" placeholder="Curso">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden" name="rota" value="cad_curso"> 
                <button type="submit" class="btn btn-primary waves-effect waves-light"> Cadastrar </button>
                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5 borda"> <a href="inicial.php?pos=1&pgs=templates/inicial_admin.php&id=inicial_admin" class="cancelar"> Cancelar </button>
            </div>
        </div>
    </form>
</section>