<section class="col-sm-8 postagem">
    <div class="bg-feed"> 
        <div class="dropdown pull-right">
            <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-settings"></i>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a onclick="Mudarestado()">Editar</a></li>
                <li><a href="../controller/postagem_controller.php?id_postagem=<?=$prof['id_postagem']?>&rota=deletar_postagem">Apagar</a></li>
            </ul>
        </div>       
        <div class="comment">

            <?php foreach(pegarImagem($_SESSION['id_usuario']) as $imagem){
                
                if($imagem!=null){ ?>

                     <img src="../<?=$imagem['caminho_imagem'];?>" alt="imagem" class="comment-avatar post">

                <?php } else{ ?>

                    <img src="assets/images/profile.jpg" class="img-thumbnail" alt="profile-image">

                <?php }}
            ?>

            <div class="comment-body post">
                <div class="comment-text" id="comentarioOriginal" style="display: block;">
                    <div class="comment-header">
                        <a href="#" title=""> <?php echo $_SESSION['nome'] ?> </a><span> <?php echo $prof['hora_postagem'];?> </span>
                    </div>
                    <?php echo $prof['texto_postagem']; ?>
                    <div>
                        <?php foreach(buscaMidiaPost($prof['id_postagem']) as $midia){ ?>
                            <img src="/assets/images/pdf-icon-2.png">
                            <object data="../<?=$midia['caminho_imagem'];?>"></object>

                        <?php }  ?>
                    </div>

                </div>
                <div class="comment-text editar" id="editarComentario" style="display: none;">
                    <form action="../controller/postagem_controller.php" method="post">
                        <span class="input-icon icon-right">
                            <input type="text" name="texto_postagem" value="<?=$prof['texto_postagem'];?>" rows="2" class="form-control">
                        </span>
                        <div class="p-t-10 pull-right">
                            <input type="hidden" name="rota" value="atualizar_postagem">
                            <input type="hidden" name="id_postagem" value=<?=$prof['id_postagem']?>>
                            <button type="submit" class="btn btn-xs btn-primary waves-effect waves-light publicar"> Alterar </button>
                        </div>
                    </form>
                </div>

                <?php 

                    $comentario = buscaComentario($prof['id_postagem']);

                    foreach($comentario as $coment) { ?>

                        <div class="comment">

                            <?php if($coment['id_usuario']==$_SESSION['id_usuario']){ ?>
                                <div class="dropdown pull-right">
                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                        <i class="zmdi zmdi-settings"></i>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Editar</a></li>
                                        <li><a href="../controller/postagem_controller.php?id_comentario=<?=$coment['id_comentario']?>&rota=deletar_comentario">Apagar</a></li>
                                    </ul>
                                </div> 
                            <?php } 

                             foreach(pegarImagem($coment['id_usuario']) as $imagem){
                                if($imagem!=null){ ?>

                                     <img src="../<?=$imagem['caminho_imagem'];?>" alt="imagem" class="comment-avatar post"s>

                                <?php } else{ ?>

                                    <img src="assets/images/profile.jpg" class="img-thumbnail" alt="profile-image">

                                <?php }}
                             ?>
                            <div class="comment-body post">
                                <div class="comment-text">
                                    <div class="comment-header">
                                        <a href="#" title=""> <?php $nome = buscaNomePorId($coment['id_usuario']); echo $nome['nome']; ?> </a>
                                        <span> <?php echo $coment['hora_comentario']; ?> </span>
                                    </div>

                                    <?php echo $coment['texto_comentario']; ?>
                                    
                                </div>
                            </div>
                        </div>
                      
                   <?php }?>

                <div class="comment-footer postagem">
                    <a onclick="adicionar_comentario()"><button class="btn btn-trans btn-xs w-xs">Comentar</button></a>
                </div>
            </div>
            <div class="col-sm-10 caixa_comentario" style="display: none;"  id="comentario_postagem">    
                <form action="../controller/postagem_controller.php" method="post">
                    <span class="input-icon icon-right">
                        <textarea name="texto_comentario" rows="2" class="form-control" placeholder="Publique algo"></textarea>
                    </span>
                    <button class="btn btn-success waves-effect waves-light btn-sm m-b-5 float" type="submit">Enviar</button>
                    <input type="hidden" name="id_postagem" value=<?=$prof['id_postagem']?>>
                    <input type="hidden" name="rota" value="cad_comentario">

                </form>    
            </div>
    </div>
</section>