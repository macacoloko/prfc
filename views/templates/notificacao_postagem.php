<ul class="list-group list-no-border user-list">
    <li class="list-group-item">
        <a href="#" class="user-list-item">
            <div class="user-desc">
                <span class="name"><?php 
                $desc = pegarDisciplinaPorId($atividades['id_disciplina']);

                foreach ($desc as $disciplina) {
                   
                echo strtoupper($atividades['tipo_atividade'])." DE ".strtoupper(($disciplina['desc_disciplina']));   

     
                } ?>
                    
                </span>

                <span class="desc">
                <?php 
                    if($_SESSION['id_tip_usuario']==1){
                        echo $atividades['texto_atividade'];
                    }elseif($_SESSION['id_tip_usuario']==2){
                        $desc_turma = buscaTurmaPorId($atividades['id_turma']);
                        echo $desc_turma['desc_turma'];
                } ?>
                </span>
                <span class="time notificacao"><?=date('d/m/Y', strtotime($atividades['data_atividade'])); ?></span>
            </div>
        </a>
    </li>
</ul>
