<div class="container">

    <!-- LOGO -->
    <div class="topbar-left">
        <a href="index.html" class="logo"><span>Virtual<span>Class</span></span></a>
    </div>
    <!-- End Logo container-->


    <div class="menu-extras">

        <ul class="nav navbar-nav navbar-right pull-right">
            <li>
                <form role="search" class="navbar-left app-search pull-left hidden-xs">
                     <input type="text" placeholder="Search..." class="form-control">
                     <a href=""><i class="fa fa-search"></i></a>
                </form>
            </li>
            <li>
                <!-- Notification -->
                <div class="notification-box">
                    <ul class="list-inline m-b-0">
                        <li>
                            <a href="javascript:void(0);" class="right-bar-toggle">
                                <i class="zmdi zmdi-notifications-none"></i>
                            </a>
                            <div class="noti-dot">
                                <span class="dot"></span>
                                <span class="pulse"></span>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- End Notification bar -->
            </li>

            <li class="dropdown user-box">
                <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown" aria-expanded="true">
                    <?php foreach(pegarImagem($_SESSION['id_usuario']) as $imagem){
                        if($imagem!=null){ ?>

                             <img src="../<?=$imagem['caminho_imagem'];?>" alt="imagem" class="img-circle user-img">

                        <?php } else{ ?>

                            <img src="assets/images/users/avatar-1.jpg" class="img-circle user-img" alt="profile-image">

                        <?php }}
                    ?>
                </a>

                <ul class="dropdown-menu">
                    <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Perfil</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Configurações</a></li>
                    <li><a href="inicial.php?pos=1&pgs=templates/upload_foto.php&id=upload_foto"><i class="ti-lock m-r-5"></i> Alterar foto</a></li>
                    <li><a href="../controller/usuario_controller.php?rota=logout"><i class="ti-power-off m-r-5"></i> Sair</a></li>
                </ul>
            </li>
        </ul>
        <div class="menu-item">
            <!-- Mobile menu toggle-->
            <a class="navbar-toggle">
                <div class="lines">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </a>
            <!-- End mobile menu toggle-->
        </div>
    </div>

</div>




