<section class="col-sm-8 form">
    <h4 class="header-title m-t-0 m-b-30">Disciplinas</h4>
    <form class="form-horizontal" role="form" action="../controller/disciplina_controller.php" method="POST" data-parsley-validate novalidate>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Disciplina</label>
            <div class="col-sm-7">
                <input type="text" name="desc_disciplina" required parsley-type="text" class="form-control"
                       id="inputEmail3" placeholder="Matéria">
            </div>
        </div>
        <div class="form-group clearfix">
            <label class="col-sm-4 control-label"> Turma(s) </label>
            <div class="col-sm-7">
                <select class="select2 select2-multiple" multiple="multiple" multiple data-placeholder="Turmas" id="select_turma" name='turma[]'>
                    
                        <?php                                                                  
                        foreach (pegarTurma() as $turma) {?>
                           
                        <option value= '<?= $turma['id_turma'];?>'> <?= utf8_encode($turma['desc_turma']); ?></option>

                        <?php } ?>
                        
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden" name="rota" value="cadastrarDisciplina"> 
                <button type="submit" class="btn btn-primary waves-effect waves-light">
                    Cadastrar
                </button>
                <button type="reset"
                        class="btn btn-default waves-effect waves-light m-l-5">
                    Cancelar
                </button>
            </div>
        </div>
    </form>
</section>