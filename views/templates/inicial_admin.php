<h4 class="header-title m-t-0 m-b-30">Editar dados</h4>
<div class="col-md-12">
    <div class="card-box">
        <div class="dropdown pull-right">
            <button type="button" class="btn btn-inverse waves-effect w-sm waves-light m-b-5 adicionar"> <a class="admin" href="inicial.php?pos=1&pgs=templates/form-novamateria.php&id=formnovamateria"> Adicionar </button>
            <button type="button" class="btn btn-danger waves-effect w-sm waves-light m-b-5"> <a class="admin" href="inicial.php?pos=1&pgs=templates/form-removerdisciplina.php&id=formremoverdisciplina"> Remover </button>
        </div>

        <h4 class="header-title m-t-0 m-b-30">Disciplinas</h4>
    </div>
</div>
<div class="col-md-12">
    <div class="card-box">
        <div class="dropdown pull-right">
            <button type="button" class="btn btn-inverse waves-effect w-sm waves-light m-b-5 adicionar"> <a class="admin" href="inicial.php?pos=1&pgs=templates/form-novaturma.php&id=formnovaturma"> Adicionar </button>
            <button type="button" class="btn btn-danger waves-effect w-sm waves-light m-b-5"> <a class="admin" href="inicial.php?pos=1&pgs=templates/form-removerturma.php&id=formremoverturma"> Remover</button>
        </div>

        <h4 class="header-title m-t-0 m-b-30">Turmas</h4>
    </div>
</div>
<div class="col-md-12">
    <div class="card-box">
        <div class="dropdown pull-right">
            <button type="button" class="btn btn-inverse waves-effect w-sm waves-light m-b-5 adicionar"> <a class="admin" href="inicial.php?pos=1&pgs=templates/form-novocurso.php&id=formnovocurso"> Adicionar </button>
            <button type="button" class="btn btn-danger waves-effect w-sm waves-light m-b-5"> Remover </button>
        </div>

        <h4 class="header-title m-t-0 m-b-30">Curso</h4>
    </div>
</div>