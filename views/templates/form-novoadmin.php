<section class="col-sm-8 form">
    <h4 class="header-title m-t-0 m-b-30">Novo administrador</h4>
    <form class="form-horizontal" role="form" action="../controller/usuario_controller.php" method="POST" data-parsley-validate novalidate>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Nome</label>
            <div class="col-sm-7">
                <input type="text" name="nome" required parsley-type="text" class="form-control" id="inputEmail3" placeholder="Nome">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Data de nascimento</label>
            <div class="col-sm-7">
                <input type="date" name="data_nasc" required parsley-type="text" class="form-control" id="inputEmail3">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
            <div class="col-sm-7">
                <input type="email" name="email" required parsley-type="text" class="form-control" id="inputEmail3" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Login</label>
            <div class="col-sm-7">
                <input type="text" name="login" required parsley-type="text" class="form-control" id="inputEmail3" placeholder="Login">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Senha</label>
            <div class="col-sm-7">
                <input type="password" name="senha" required parsley-type="text" class="form-control" id="inputEmail3" placeholder="Senha">
            </div>
        </div>
        <div class="form-group">
        	<input type="hidden" name="rota" value="cad_admin"> 
        	<ul class="pager wizard m-b-0">
	            <button type="submit" class="btn btn-primary waves-effect waves-light">
	                Cadastrar
	            </button>
	        </ul>
		</div>
    </form>
</section>