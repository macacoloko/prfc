<div class="col-md-5 col-sm-8 col-xs-12 centralizado">
    <div class="card-box p-b-0 box_login">        
        <div id="basicwizard" class=" pull-in">
            <ul>
                <li><a href="#tab1" data-toggle="tab">Login</a></li>
                <li><a href="#tab2" data-toggle="tab">Cadastre-se</a></li>
            </ul>
            <div class="tab-content b-0 m-b-0">
                <div class="tab-pane m-t-10 fade" id="tab1">
                    <div class="row">
                        <form action="../controller/usuario_controller.php" method="post">
                            <div class="form-group clearfix">
                                <label class="col-md-3 control-label " for="userName"> <i class="glyphicon glyphicon-user"></i> Usuário </label>
                                <div class="col-md-9">
                                    <input class="form-control required" type="text" name="login" placeholder="Usuário">
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <label class="col-md-3 control-label " for="password"> <i class="glyphicon glyphicon-lock"></i> Senha </label>
                                <div class="col-md-9">
                                    <input id="password" name="senha" type="password" class="required form-control" placeholder="Senha">
                                </div>
                            </div>
                            <input type="hidden" name="rota" value="logar">
                            <ul class="pager wizard m-b-0">
                                <button class="btn btn-inverse btn-bordred waves-effect w-md waves-light m-b-5" type="Submit"> Logar</button>
                            </ul>
                        </form>
                    </div>
                </div>

                <div class="tab-pane m-t-10 fade" id="tab2">
                    <div class="row">
                        <form action="../controller/usuario_controller.php" method="post">
                            <div class="form-group clearfix">
                                <label class="col-lg-2 control-label" for="name"> Nome </label>
                                <div class="col-lg-10">
                                    <input id="name" name="nome" type="text" pattern=".{12,50}" title="Informe seu nome completo" class="required form-control" placeholder="Nome">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-lg-2 control-label " for="surname"> Email </label>
                                <div class="col-lg-10">
                                    <input id="email" name="email" type="email" class="required form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-lg-2 control-label " for="Date"> Data de nascimento </label>
                                <div class="col-lg-10">
                                    <input name="data_nasc" type="date" placeholder="" class="form-control">
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="col-lg-2 control-label" for="email"> Usuário </label>
                                <div class="col-lg-4">
                                    <input id="login" name="login" type="text" class="required email form-control meio" placeholder="Usuário">
                                </div>
                                <label class="col-lg-2 control-label" for="email"> Senha </label>
                                <div class="col-lg-4">
                                    <input id="senha" name="senha" type="password" pattern=".{8,12}" title="Senha deve conter de 8 a 12 caracteres" class="required email form-control meio" placeholder="Senha">
                                </div>
                            </div>
                            
                            <div class="form-group clearfix">
                                <label class="col-lg-2 control-label"  for="email"> Tipo de usuário </label>
                                <div class="col-lg-10">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="radio">
                                                <input type="radio" name="id_tip_usuario" id="tipo_usuario_2" value= 2>
                                                <label for="radio1">
                                                    Professor
                                                </label>
                                                <input type="radio" name="id_tip_usuario" id="tipo_usuario_1" value= 1>
                                                <label for="radio03">
                                                    Aluno
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <label class="col-lg-2 control-label"> Status </label>
                                <div class="col-lg-10">
                                    <select class="form-control select2" name="status" id='select_status'>
                                        <option></option>                                       
                                    </select>
                                </div>
                            </div>

                            <div class="form-group clearfix" style="display: none;" id="turmaprofessor">
                                <label class="col-lg-2 control-label"> Turma(s) </label>
                                <div class="col-lg-10">
                                    <select class="select2 select2-multiple" multiple="multiple" placeholder="Turmas em que leciona" id="select_turma" name='turma[]'>
                                        
                                            <?php                                                                  
                                            foreach (pegarTurma() as $turma) {?>

                                               
                                            <option value= '<?=$turma['id_turma'];?>'> <?=utf8_encode($turma['desc_turma']);?></option>

                                            <?php } ?>
                                            
                                    </select>
                                </div>
                            </div>
                            <div class="form-group clearfix" style="display: none;" id="turmaaluno">
                                <label class="col-lg-2 control-label"> Turma </label>
                                <div class="col-lg-10">
                                    <select class="form-control select2" id="select_turma" name='turma[]'>
                                        
                                            <?php                                                                  
                                            foreach (pegarTurma() as $turma) {?>
                                               
                                            <option value= '<?= $turma['id_turma'];?>'> <?= utf8_encode($turma['desc_turma']); ?></option>

                                            <?php } ?>
                                            
                                    </select>
                                </div>
                            </div>
                            <div class="form-group clearfix" style="display: none;" id="disc_prof">
                                <label class="col-lg-2 control-label"> Disciplinas </label>
                                <div class="col-lg-10">
                                    <select class="select2 select2-multiple" multiple="multiple" placeholder="Disciplinas que aplica" id="select_turma" name='disciplinas[]'>
                                        <?php foreach (pegarTodasDisciplinas() as $disciplina){ ?>
                                            <option value="<?= $disciplina['id_disciplina']; ?>"><?= $disciplina['desc_disciplina']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="rota" value="cad_usuario">
                            <ul class="pager wizard m-b-0">
                                <button class="btn btn-inverse btn-bordred waves-effect w-md waves-light m-b-5" type="Submit"> Cadastrar </button>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

               
           
<!-- end card-box -->

<div class="row">
	<div class="col-sm-12 text-center">
		<p class="text-muted">Already have account?<a href="page-login.html" class="text-primary m-l-5"><b>Sign In</b></a></p>
	</div>
</div>


<!-- end wrapper page -->
