<section class="col-sm-8 form">
    <h4 class="header-title m-t-0 m-b-30">Remover Disciplina</h4>
    <form class="form-horizontal" role="form" action="../controller/disciplina_controller.php" method="POST" data-parsley-validate novalidate>
        <div class="form-group clearfix">
            <label class="col-sm-4 control-label"> Disciplinas </label>
            <div class="col-sm-7">
                <select class="select2 select2-multiple" multiple="multiple" multiple data-placeholder="Turmas" id="select_turma" name='disciplina[]'>
                    
                        <?php                                                                  
                        foreach (pegarDisciplina() as $disciplina) {?>
                           
                        <option value= '<?= $disciplina['id_disciplina'];?>'> <?= utf8_encode($disciplina['desc_disciplina']); ?></option>

                        <?php } ?>
                        
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <input type="hidden" name="rota" value="removerDisciplina"> 
                <button type="submit" class="btn btn-primary waves-effect waves-light">
                    Remover
                </button>
                <button type="reset"
                        class="btn btn-default waves-effect waves-light m-l-5">
                    Cancelar
                </button>
            </div>
        </div>
    </form>
</section>