<form action="../controller/postagem_controller.php" enctype="multipart/form-data" method="POST" class="card-box">
    <span class="input-icon icon-right">
        <textarea name="texto_postagem" rows="2" class="form-control" placeholder="Publique algo"></textarea>
    </span>
    
    <ul class="nav nav-pills profile-pills m-t-10">

        <div class="col-sm-4 post">
            <select class="select2 select2-multiple" multiple="multiple" multiple data-placeholder="Turmas" id="select_turma" name='turma[]'>

                <?php

                foreach (buscaTurmaUsuario($_SESSION['id_usuario']) as $turmas) { ?>

                <option value= '<?= $turmas['id_turma'];?>'> <?=$turmas['desc_turma']; ?></option>

                <?php } ?>

            </select>
        </div>

        <div class="col-sm-4 post">
            <select class="form-control select2" placeholder="disciplina"  id="select_disciplina" name='disciplina[]'>

            <?php

                foreach (buscaDisciplinaUsuario($_SESSION['id_usuario']) as $disciplina): ?>

                    <option value= '<?= $disciplina[0]['id_disciplina'];?>'> <?=$disciplina[0]['desc_disciplina']; ?></option>

                <?php endforeach ?>       

            </select>
        </div>
       <!--  <div class="col-sm-2">
            <span class="select-wrapper">
                <input type="file" name="image_src" id="image_src" />
            </span>
        </div> -->
        <fieldset class="infraFieldset">
        <label id="lblArquivo" for="txtArquivo" class="infraLabelObrigatorio"></label>
        <input type="file" id="txtArquivo" name="txtArquivo" value="" placeholder="anexar doc" />
        </fieldset>
        
        <div class="p-t-10 pull-right">
            <input type="hidden" name="rota" value="cad_postagem">
            <button type="submit" class="btn btn-primary waves-effect waves-light publicar"> Publicar </button>
        </div>



    </ul>

</form>

