<div class="container">
    <div id="navigation">
        <!-- Navigation Menu-->
        <ul class="navigation-menu">
            <?php if($_SESSION['id_tip_usuario']==3){ ?>
            <li>
                <a href="inicial.php?pos=1&pgs=templates/inicial_admin.php&id=inicial_admin"><i class="zmdi zmdi-view-dashboard"></i> <span> Início </span> </a>
            </li>
            <?php } else { ?>
            <li>
                <a href="inicial.php"><i class="zmdi zmdi-view-dashboard"></i> <span> Início </span> </a>
            </li>
            <?php } ?>

            <?php if($_SESSION['id_tip_usuario']==1){ ?>
            <li class="has-submenu">
                <a href="#"><i class="zmdi zmdi-invert-colors"></i> <span> Matérias </span> </a>
                <ul class="submenu megamenu">
                    <li>
                        <ul>
                            <?php

                            foreach (buscaTurmaUsuario($_SESSION['id_usuario']) as $turma) {

                               foreach (disciplinaTurma($turma['id_turma']) as $disciplinas) {?>

                                <li><a href=""> <?=$disciplinas['desc_disciplina'];?> </a></li>
                               
                                <?php } 
                            }

                            ?>
                        </ul>
                    </li>
                    <li>
                        <ul>
                            <li><a href=""></a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="has-submenu">
                <a href="inicial.php?pos=1&pgs=templates/calendario.php&id=calendario"><i class="zmdi zmdi-calendar"></i><span> Calendário </span> </a>
            </li>

            <?php } elseif ($_SESSION['id_tip_usuario']==2) {?>

            <li class="has-submenu">
                <a href="#"><i class="zmdi zmdi-calendar"></i><span> Turmas </span> </a>
                <ul class="submenu megamenu turmas">
                    <li>
                        <ul>
                            <?php
 

                               foreach (buscaTurmaUsuario($_SESSION['id_usuario']) as $turma) {?>

                                <li><a href=""> <?=$turma['desc_turma'];?> </a></li>
                               
                                <?php } 

                            ?>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="has-submenu">
                <a href="#"><i class="zmdi zmdi-calendar"></i><span> Atividade </span> </a>
                <ul class="submenu megamenu turmas">
                    <li>
                        <ul>
                            <li><a href="inicial.php?pos=1&pgs=templates/form-novaatividade.php&id=form-novaatividade">Marcar atividade </a></li>
                            <li><a href="inicial.php?pos=1&pgs=templates/form-removeratividade.php&id=form-removeratividade">Remover atividade </a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <?php } else{ ?>

            <li class="has-submenu">

                <a href="inicial.php?pos=1&pgs=templates/form-novoadmin.php&id=formnovoadmin"> <i class="zmdi zmdi-account-add"></i><span> Novo admin </span> </a>
            </li>

            <!--<li class="has-submenu">

                <a href="inicial.php?pos=1&pgs=templates/form-novamateria.php&id=formnovamateria"> <i class="zmdi zmdi-calendar"></i><span> Disciplinas </span> </a>
            </li>
            <li class="has-submenu">

                <a href="inicial.php?pos=1&pgs=templates/form-novaturma.php&id=formnovamaturma"> <i class="zmdi zmdi-calendar"></i><span> Turmas </span> </a>
            </li>
            <li class="has-submenu">

                <a href="inicial.php?pos=1&pgs=templates/form-novocurso.php&id=formnovocurso"> <i class="zmdi zmdi-calendar"></i><span> Cursos </span> </a>
            </li>-->
            <?php } ?>

        </ul>
        






        <!-- End navigation menu  -->

    </div>
</div>