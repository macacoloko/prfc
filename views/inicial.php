<?php  
include_once'../controller/pagina_controller.php'; 
include_once '../controller/midia_controller.php';
include_once '../controller/disciplina_controller.php';
include_once '../controller/turma_controller.php';
include_once '../controller/postagem_controller.php';
include_once '../controller/atividade_controller.php';

@session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title>Adminto - Responsive Admin Dashboard Template</title>

        <!-- css do calendario -->

        <link href="assets/plugins/fullcalendar/dist/fullcalendar.css" rel="stylesheet" />
        <!-- fim calendario -->

        <link href="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
        <link href="assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        <link href="assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/estilo.css" rel="stylesheet" type="text/css">
        <style type="text/css">
        .select-wrapper {
              background: url(http://s10.postimg.org/4rc0fv8jt/camera.png) no-repeat;
              background-size: cover;
              display: block;
              position: relative;
              width: 33px;
              height: 26px;
            }
            #image_src {
              width: 26px;
              height: 26px;
              margin-right: 100px;
              opacity: 0;
              filter: alpha(opacity=0); /* IE 5-7 */
                }
        </style>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>


    </head>
    <body class="inteiro">

        <header id="topnav">
            <div class="topbar-main">
                <?php 
                
                    include 'templates/menu.php';
                
                ?>                 
            </div>

            <div class="navbar-custom">
                <?php 
                    include 'templates/nav.php';            
                ?>                              
            </div>
        </header>
        

        <div class="wrapper topo">
            <div class="container">

                <section class="row">
                    <section class="col-sm-4 inteiro">
                        <div class="bg-picture card-box">
                            <?php include 'templates/info-perfil.php' ?>                       
                        </div>
                        <!-- <div class="card-box mini-calendar">
                            <div id="datepicker-inline">                            
                                  FALTA DEIXAR RESPONSIVO
                            </div>
                        </div> -->
                    </section>

                    <?php if(empty($_GET['id'])){ 

                            if ($_SESSION['id_tip_usuario']==2){ ?>

                                <section class="col-sm-8"> 
                                   
                                        <?php require 'templates/postagem.php'; ?>
                                   
                                </section>

                        <?php   

                        foreach (postagemProf($_SESSION['id_usuario']) as $prof) { 

                                include 'templates/postagem-professor.php';                                                        
                            }

                            } elseif($_SESSION['id_tip_usuario'] == 1) {

                                foreach (postagemTurma($_SESSION['id_usuario']) as $post) {

                                    foreach ($post as $post_turma) {
                                       include 'templates/postagem-turma.php';    
                                    }

                                                                                 
                                }

                             }else{ ?>
                                    
                                    <section class="col-sm-8">
                                        <div class="bg-feed">
                                            
                                            <?php include'templates/inicial_admin.php'; ?>

                                        </div>
                                    </section> 
                        
                            <?php }} else{ ?> 

                                <section class="col-sm-8">
                                    <div class="bg-feed">

                                    <?php ins_dados(filter_input(INPUT_GET, 'pos'),filter_input(INPUT_GET, 'pgs')); ?>

                                    </div>
                                </section>
                            <?php } ?>
                

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->
                </section>

            </div>
            <!-- end container -->



            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="zmdi zmdi-close-circle-o"></i>
                </a>
                <?php if($_SESSION['id_tip_usuario']==1){

                    $turma = buscaTurmaUsuario($_SESSION['id_usuario']);

                    foreach ($turma as $id_turma) { 
                        
                        $atividade = buscaAtividade($id_turma['id_turma']);

                        foreach ($atividade as $atividades) {

                            $ativ = $atividades['data_atividade'];

                            $data = proxima_semana($ativ); ?>

                                    <h4 class="">Notificações</h4>
                                    <div class="notification-list nicescroll notificacao">
                                            
                                        <?php 

                                        if($data!="não"){
                                            if($data=="sim"){ ?>        

                                            <h4 class="">Esta semana:</h4>
                                         
                                            <?php include 'templates/notificacao_postagem.php'; 
                                            }else { ?>

                                            <h4 class="">Proximas semanas:</h4>
                                    
                                            <?php include 'templates/notificacao_postagem.php'; ?>
                                                
                                            <?php } 
                                        } ?>
                                    </div>
                                <?php
                            }
                        }
                    }
                    elseif ($_SESSION['id_tip_usuario']==2){

                    foreach (buscaAtividadeProf($_SESSION['id_usuario']) as $atividades) {?>
                        
                    <h4 class="">Notificações</h4>
                    <div class="notification-list nicescroll">  
                    
                        <?php include 'templates/notificacao_postagem.php'; ?>

                    </div>

                    <?php }
                }  ?>
                                      
            </div>
          

        </div>



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
        <script src="assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="assets/plugins/moment/moment.js"></script>
        <script src="assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <script src="assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>



        <!-- App js -->


        <script src="assets/js/script-inicial.js"></script>
        <script src="assets/js/comentario.js"></script>
        <script src="assets/js/campo_editavel.js"></script>

        <!-- Jquery-Ui -->
        <script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>

        <!-- BEGIN PAGE SCRIPTS -->
        <script src="assets/plugins/moment/moment.js"></script>
        <script src='assets/plugins/fullcalendar/dist/fullcalendar.min.js'></script>
        <script src="assets/pages/jquery.fullcalendar.js"></script>
        <script src="assets/pages/jquery.fullcalendar.js"></script>




        <!-- Modal-Effect -->
        <script src="assets/plugins/custombox/dist/custombox.min.js"></script>
        <script src="assets/plugins/custombox/dist/legacy.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script>

        $(document).ready(function() {

            $('#calendario').fullCalendar({
                lang: 'pt-br'
            });

        });

    </script>

    </body>
</html>