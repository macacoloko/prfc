<?php require '../models/Pagina.class.php';
    require 'funcao.php';



function pegarTurma(){

	$turma = Pagina::buscarTurma();
	
	return $turma;
}
function disciplinaTurma($id_turma){

    $turma = $id_turma;

    $disciplinas = Pagina::buscarDisciplina($turma);

    return $disciplinas;
}

function pegarTodasDisciplinas(){
    $disciplina = Pagina::buscarTodasDisciplinas();
    return $disciplina;

}
function buscaDisciplinaUsuario($id_usuario){
    $disciplinaUsu = Pagina::buscaDisciplinaUsuario($id_usuario);
    return $disciplinaUsu;

}
function ins_dados($pos,$pgs){
    switch ($pos){
        case 1:
            include_once $pgs;
            break;
        case 2:
            include_once $pgs;
            break;
        case 3:
            include_once $pgs;
            break;
        default :
            echo'Item não encontrado';
            break;
    }
}
function buscaTurmaUsuario($id_usuario){

    $turmaUsuario = Pagina::buscaTurmaUsuario($id_usuario);

    return $turmaUsuario;
}

function postagemTurma($id_usuario){


    $turma = buscaTurmaUsuario($id_usuario);

    foreach ($turma as $id) {
        
        $id_turma = $id['id_turma'];

        arrayValuesToInt($id_turma);
    } 

    $postagem = Pagina::buscaPostagemTurma($id_turma);

    return $postagem;
}

function buscaNomePorId($id_usuario){

    $nome = Pagina::buscaNomePorId($id_usuario);

    return $nome;
}

function pegarImagem($id){
    $imagem = Pagina::buscarImagem($id);
    return $imagem;
}



if (@function_exists($_POST['rota'])) {
    call_user_func($_POST['rota']);
}

if (@function_exists($_GET['rota'])) {
    call_user_func($_GET['rota']);
}

