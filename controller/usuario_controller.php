<?php

	require_once '../models/Usuario.class.php';
	include_once 'funcao.php';
	require_once 'valida_campos.php';
	session_start();

	function cad_usuario(){

		$valida = validaCampos($_POST);

		if($valida!="ok"){

			var_dump($valida);
			die();

		}else{

			$nome        = $_POST['nome'];
			$email       = $_POST['email'];
			$data_nasc   = $_POST['data_nasc'];
			$senha       = $_POST['senha'];
			$login       = $_POST['login'];
			$status      = $_POST['status'];
			$tip_usuario = $_POST['id_tip_usuario'];

			$usuario = new Usuario();

			$deu_certo = $usuario->cadastrar($nome, $email, $data_nasc, $senha, $login, $status, $tip_usuario);

			if ($deu_certo) {
				
				$resultado = Usuario::busca_usuario($login, $senha);
				
				foreach ($resultado as $value){
					arrayValuesToInt($value);
					$id_usu = $value['id_usuario'];
				
				} 

				if ($id_usu) {
					$turmas = $_POST['turma'];
					arrayValuesToInt($turmas);
					$id_usuario = $id_usu;	
					$usuario->insere_turma($id_usuario, $turmas);
			
				} if($tip_usuario==2){

					$disciplinas = $_POST['disciplinas'];

					foreach ($resultado as $result) {

						$relaciona = Usuario::relacionaProfDisciplina($result['id_usuario'], $disciplinas);	
					}
				} 
				
				logar();
			
			}else{

				header('locaton:../views/templates/index.php');

			}
		}
	}

	function cad_admin(){

		$nome = $_POST['nome'];
		$data_nasc = $_POST['data_nasc'];
		$email = $_POST['email'];
		$login = $_POST['login'];
		$senha = $_POST['senha'];

		$admin = new Usuario();

		$admin->cadastrarAdmin($nome, $data_nasc, $email, $login, $senha);

		if(true){
			header('location:../views/inicial.php?pos=1&pgs=templates/inicial_admin.php');
		}else {
			echo "erro";
			die();
		}
	}

	function logar(){

	    $login = $_POST['login'];
	    $senha = $_POST['senha'];

	    $usuario = Usuario::efetuarLogin($login, $senha);


	    if ($usuario['id_tip_usuario']==3){
	        $_SESSION['id_usuario'] = $usuario['id_usuario'];
	        $_SESSION['nome'] = $usuario['nome'];
	        $_SESSION['email'] = $usuario['email'];
	        $_SESSION['data_nasc'] = $usuario['data_nasc'];
	        $_SESSION['login'] = $usuario['login'];
	        $_SESSION['senha'] = $usuario['senha'];
	        $_SESSION['id_tip_usuario'] = $usuario['id_tip_usuario'];
	        $_SESSION['status'] = $usuario['status'];

	        header('location:../views/inicial.php?pos=1&pgs=templates/inicial_admin.php&id=inicial_admin');

	    } elseif ($usuario['id_tip_usuario']==1 OR $usuario['id_tip_usuario']==2){

	    	$_SESSION['id_usuario'] = $usuario['id_usuario'];
	        $_SESSION['nome'] = $usuario['nome'];
	        $_SESSION['email'] = $usuario['email'];
	        $_SESSION['data_nasc'] = $usuario['data_nasc'];
	        $_SESSION['login'] = $usuario['login'];
	        $_SESSION['senha'] = $usuario['senha'];
	        $_SESSION['id_tip_usuario'] = $usuario['id_tip_usuario'];
	        $_SESSION['status'] = $usuario['status'];

	        header('location:../views/inicial.php');
	    }
	    else{

	        unset($_SESSION['id_tip_usuario']);
	        unset($_SESSION['email']);
	        unset($_SESSION['nome']);
	        unset($_SESSION['idade']);
	        unset($_SESSION['id']);
	        unset($_SESSION['login']);
	        unset($_SESSION['senha']);
	        header('location:../views/templates/cadastro.php');
	    }
	}

	function logout (){

		unset($_SESSION['login']);
		unset($_SESSION['senha']);

		session_destroy();

		header('location:../views/index.php');
	}


if (@function_exists($_POST['rota'])) {
	call_user_func($_POST['rota']);
}

if (@function_exists($_GET['rota'])) {
	call_user_func($_GET['rota']);
}	