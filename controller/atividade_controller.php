<?php 
require "../models/Atividade.class.php";
@session_start();

function cad_atividade(){


	$id_usuario      = $_SESSION['id_usuario'];
	$texto_atividade = $_POST['descricao'];
	$id_turma        = $_POST['turma'];
	$tipo_atividade  = $_POST['tipo_atividade'];
	$id_disciplina   = $_POST['disciplina'];
	$data_atividade  = $_POST['data_atividade'];

	$atividade = new Atividade();

	$cadastro_ativ = $atividade->cad_atividade($id_usuario, $texto_atividade, $id_turma, $tipo_atividade, $id_disciplina, $data_atividade);

	if($cadastro_ativ){
		header('location:../views/inicial.php');
	}else{
		echo "errrrrrrrrrrrrrrr";
		die();
	}
} 
function buscaAtividade($id_turma){

	$atividade = Atividade::buscaAtividade($id_turma);

	return $atividade;
}
function buscaAtividadeProf($id_usuario){

	$atividade = Atividade::buscaAtividadeProf($id_usuario);

	return $atividade;
}
function remove_atividade(){

	$id_atividade = $_POST['id_atividade'];

	$atividade = Atividade::removeAtividade($id_atividade);

	
	header('location:../views/inicial.php?pos=1&pgs=templates/form-removeratividade.php&id=form-removeratividade');
	
}
function proxima_semana($data_atividade){


	date_default_timezone_set('America/Sao_Paulo');

	$data1 = new DateTime();

	$dataatual = $data1->format('d-m-Y');

	$semana = new DateTime('+1 week');

	$semana = $semana->format('d-m-Y');

	$data2 = new DateTime($data_atividade);

	$data_ativ = $data2->format('d-m-Y');

	if($data_ativ<=$semana and $data_ativ>=$dataatual){
		$resultado1 = "sim";
		return $resultado1;
	}elseif($data_atividade>$semana){
		$resultado2 = "proxima";
		return $resultado2;
	}else{
		$resultado3 = "não";
		return $resultado3;
	}
	
	
}


if (@function_exists($_POST['rota'])) {
    call_user_func($_POST['rota']);
}

if (@function_exists($_GET['rota'])) {
    call_user_func($_GET['rota']);
}



