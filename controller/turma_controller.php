<?php 
require '../models/Turma.class.php';
include_once 'funcao.php';


function cadastrarTurma(){

	$desc_turma = strtoupper($_POST['turma']);
	$curso = $_POST['curso'];
	$disciplinas = $_POST['disciplina'];

	arrayValuesToInt($disciplinas);

	$turma = new Turma();

	$turma->cadastrarTurma($desc_turma, $curso, $disciplinas);

	if($turma){
		header('location: ../views/inicial.php?pos=1&pgs=templates/form-novaturma.php&id=formnovaturma');
	}else{
		echo "errou";
		die();
	}

}


function pegarCurso(){

	$cursos = Turma::pegarCurso();
	
	return $cursos;
}

function removerTurma(){

	$turma = $_POST['remove_turma'];

	$remove = Turma::removerTurma($turma);

	if($remove){
		header('location: ../views/inicial.php?pos=1&pgs=templates/form-removerturma.php&id=formremoverturma');
	}else{
		echo "errou";
		die();
	}

}
function buscaTurmaPorId($id_turma){

	$desc_turma = Turma::buscaTurmaPorId($id_turma);

	return $desc_turma;

}




if (@function_exists($_POST['rota'])) {
	call_user_func($_POST['rota']);
}
if (@function_exists($_GET['rota'])) {
	call_user_func($_GET['rota']);
}


?>