<?php


function validaCampos(){

	$erro = false;

	if($_POST['nome'] == ""){
		$erro = "Nome não pode ficar vazio";

	} elseif (preg_match('/[\'£$%&*()}{@#?><>,|=_+¬-]/', $_POST['nome'])) {
        $erro = "Nome não pode conter caracteres especiais";

    } if(strlen($_POST['nome']) <= 11 ){
		$erro = "Por favor coloque seu nome completo";
	} 

    if($_POST['senha'] === ""){
        $erro = "Senha não pode ficar em branco";
    } else if(strlen($_POST['senha']) > 15){
        $erro = "Senha não pode conter mais do que 15 caracteres";
    } else if(strlen($_POST['senha']) < 8){
        $erro = "Senha não pode conter menos do que 8 caracteres";
    } else if(!preg_match("/^[a-zA-Z0-9]*$/", $_POST['senha'])){
        $erro = "Senha não pode conter caracteres especiais";
    }

    if(preg_match('/[\'£$%&*()}{@#?><>,|=+¬]/',$_POST['login'])){
    	$erro = "Login só pode letras, numeros, pontos e traços";
    }


    if($erro){
		return $erro;
	}else{
		return "ok";
	}

}





