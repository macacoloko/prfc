<?php 
require '../models/Midia.class.php';
@session_start();

function cad_imagem(){

	date_default_timezone_get('America/Sao_Paulo');

	$extensao_img = array(explode('/',$_FILES['upload']['type']),date('Y-m-d-H-i-s'));
	$temp = $_FILES['upload']['tmp_name'];
	$nome_imagem = $extensao_img[1].'.'.$extensao_img[0][1];
	$caminho = 'views/assets/images/'.$nome_imagem;
	$id_usuario = $_SESSION['id_usuario'];

	$imagem = new Imagem();

	$cad_imagem = $imagem->cad_imagem($caminho, $nome_imagem, $id_usuario);
	$pasta_destino = '../views/assets/images/'.$nome_imagem;
	move_uploaded_file($temp, $pasta_destino);

	if($cad_imagem==true){

		header('location:../views/inicial.php');
	}
}

function cad_imagem_postagem($files, $id_postagem){

	date_default_timezone_get('America/Sao_Paulo');

	foreach ($files as $file) {

		$extensao_img = array(explode('/',$file['image_src']['type']),date('Y-m-d-H-i-s'));

		print_r($extensao_img);
		die();
		$temp = $file['image_src']['tmp_name'];
		$nome_imagem = $extensao_img[1].'.'.$extensao_img[0][1];
		$caminho = 'views/assets/images/'.$nome_imagem;
		$id_postagem = $id_postagem;

		$imagem_post = new Imagem();

		$img = $imagem_post->cad_imagem_postagem($caminho, $nome_imagem, $id_postagem);

		$pasta_destino = '../views/assets/images/'.$nome_imagem;
		move_uploaded_file($temp, $pasta_destino);

		}
}

if (@function_exists($_POST['rota'])) {
	call_user_func($_POST['rota']);
}
if (@function_exists($_GET['rota'])) {
		call_user_func($_GET['rota']);
	}

?>