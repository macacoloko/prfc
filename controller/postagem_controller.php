<?php
require_once '../models/Postagem.class.php';
require_once 'funcao.php';
require_once 'midia_controller.php';
@session_start();


function cad_postagem(){

	$id_usuario = $_SESSION['id_usuario'];
	$texto_postagem = $_POST['texto_postagem'];
	$turmas = $_POST['turma'];
	$disciplinas = $_POST['disciplina'];

	arrayValuesToInt($turmas);
	arrayValuesToInt($disciplinas);

	$postagem = new Postagem();

	$id_postagem = $postagem->cadastro($id_usuario,$texto_postagem);

	arrayValuesToInt($id_postagem);

	$pathToSave = "views/assets/documentos/";

	if ($_FILES) { // Verificando se existe o envio de arquivos.

	    if ($_FILES['txtArquivo']) { // Verifica se o campo não está vazio.
	        $dir = 'C:/xampp/htdocs/PROJETO/'.$pathToSave;
	        // Diretório que vai receber o arquivo.
	        $tmpName = $_FILES['txtArquivo']['tmp_name']; // Recebe o arquivo temporário.
	        
	        $name = $_FILES['txtArquivo']['name'];
	         // Recebe o nome do arquivo.
	        preg_match_all('/\.[a-zA-Z0-9]+/', $name, $extensao);
	        if (!in_array(strtolower(current(end($extensao))), array('.txt', '.pdf', '.doc', '.xls', '.xlms'))) {
	            echo('Permitido apenas arquivos doc,xls,pdf e txt.');
	            header('Location: '.suapagina.php);
	            die;
	        }

	        // move_uploaded_file( $arqTemporário, $nomeDoArquivo )
	        if (move_uploaded_file($tmpName, $dir.$name)) { // move_uploaded_file irá realizar o envio do arquivo.        
	            echo('Arquivo adicionado com sucesso.');
	        } else {
	            echo('Erro ao adicionar arquivo.');
	        }    
	    }  

	    $caminho = $pathToSave.$name;

	    $adicionar_conteudo = Postagem::adicionar_conteudo($caminho, $id_postagem, $name);
	}


	$relaciona = $postagem->relacionaPostagem($turmas, $id_postagem, $disciplinas);

	if($relaciona){
		
		header('location:../views/inicial.php');

	}else {
		echo "erro";
		die();
	}
}

function cad_comentario(){

	$id_usuario = $_SESSION['id_usuario'];
	$texto_comentario = $_POST['texto_comentario'];
	$id_postagem = $_POST['id_postagem'];

	$comentario = new Postagem();

	$cad_coment = $comentario->cad_comentario($id_usuario, $texto_comentario, $id_postagem);

	if($cad_coment){
		header('location:../views/inicial.php');
	}

}

function buscaMidiaPost($id_postagem){

	$midia = Postagem::buscaMidiaPost($id_postagem);

	return $midia;
}

function buscaComentario($id_postagem){

	arrayValuesToInt($id_postagem);

	$comentario = Postagem::buscaComentario($id_postagem);

	return $comentario;
}

function postagemProf($id_usuario){

    $dados_postagem = Postagem::postagemProf($id_usuario);

    return $dados_postagem;
}

function deletar_postagem(){

	$id_postagem = $_GET['id_postagem'];

	$deletar = Postagem::deletar_postagem($id_postagem);

	if($deletar){
		header('location:../views/inicial.php');
	}
}

function deletar_comentario(){

	$id_comentario = $_GET['id_comentario'];

	$deletar_coment = Postagem::deletar_comentario($id_comentario);

	if($deletar_coment){
		header('location:../views/inicial.php');
	}
}
function atualizar_postagem(){

	$id_postagem = $_POST['id_postagem'];
	$texto_postagem = $_POST['texto_postagem'];

	$alterar_post = Postagem::alterar_post($id_postagem, $texto_postagem);

	if($alterar_post){
		header('location:../views/inicial.php');
	}
}
function atualizar_comentario(){

	$id_comentario = $_POST['id_postagem'];
	$texto_comentario = $_POST['texto_postagem'];

	$alterar_coment = Postagem::alterar_coment($id_postagem, $texto_comentario);

	if($alterar_post){
		header('location:../views/inicial.php');
	}
}

if (@function_exists($_POST['rota'])) {
		call_user_func($_POST['rota']);
	}


if (@function_exists($_GET['rota'])) {
		call_user_func($_GET['rota']);
	}
