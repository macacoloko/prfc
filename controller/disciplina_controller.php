<?php
require '../models/Disciplina.class.php';
include_once 'funcao.php';


function cadastrarDisciplina(){

	$desc_disciplina = strtoupper($_POST['desc_disciplina']);
	$turmas = $_POST['turma'];
	
	arrayValuesToInt($turmas);

	$Disciplina = new Disciplina();
	
	$novadisciplina = $Disciplina->cadastrarDisciplina($desc_disciplina);

	arrayValuesToInt($novadisciplina);

	$possui = Disciplina::relacionaDisciplina($turmas, $novadisciplina);

	if($possui){
		header('location: ../views/inicial.php?pos=1&pgs=templates/inicial_admin.php&id=inicial_admin');
	}else{
		echo "errou";
		die();
	}

}

function pegarDisciplina(){

	$disciplinas = Disciplina::pegarDisciplina();
	
	return $disciplinas;
}

function removerDisciplina(){

	$disciplina = $_POST['disciplina'];
	arrayValuesToInt($disciplina);

	$disc = Disciplina::removerDisciplina($disciplina);

	if($disc){
		header('location: ../views/inicial.php?pos=1&pgs=templates/inicial_admin.php&id=inicial_admin');
	}else{
		echo "errou";
		die();
	}


}
function pegarDisciplinaPorId($id_disciplina){

	$disciplina = Disciplina::pegarDisciplinaPorId($id_disciplina);

	return $disciplina;

}





if (@function_exists($_POST['rota'])) {
	call_user_func($_POST['rota']);
}
if (@function_exists($_GET['rota'])) {
		call_user_func($_GET['rota']);
	}
?>
